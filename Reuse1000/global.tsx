//import { useFonts, Lato_400Regular, Lato_700Bold} from '@expo-google-fonts/lato'; 
export const style = { 
    colors: {
        dblue:'21209C',
        yellow:'EBCB2B',
        ivory:'F1F1F1',
        black:'23120B'

    },

    fonts: {
        lato400: 'Lato_400Regular',
        lato700: 'Lato_700Bold'
    } 
}