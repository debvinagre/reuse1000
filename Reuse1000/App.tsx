import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import BoasVindas from './src/pages/BoasVindas'
import { Container } from './src/pages/BoasVindas/style';

export default function App() {
  return (
    <Container>
      <BoasVindas/>
    </Container>
  );
}
